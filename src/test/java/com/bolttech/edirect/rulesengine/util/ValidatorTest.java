package com.bolttech.edirect.rulesengine.util;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidatorTest {

    @Test
    public void whenReceiveEmptyString_returnFalse() {
        boolean test = Validator.isExpiredVoucher("", "");
        assertFalse(test);
    }

    @Test
    public void isExpiredVoucher_ReceiveStringDateZoneInRange_returnFalse() {
        boolean test = Validator.isExpiredVoucher("2020-11-10T00:00:00.000Z", "2020-11-28T00:00:00.000Z");
        assertFalse(test);
    }

    @Test
    public void isExpiredVoucher_isTrue_returnTrue() {
        boolean test = Validator.isExpiredVoucher("2020-11-10T00:00:00.000Z", "2020-11-09T00:00:00.000Z");
        assertTrue(test);
    }

}
