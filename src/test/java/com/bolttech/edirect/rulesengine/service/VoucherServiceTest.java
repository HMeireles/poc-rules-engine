package com.bolttech.edirect.rulesengine.service;

import com.bolttech.edirect.rulesengine.mocks.MockUtil;
import com.bolttech.edirect.rulesengine.model.api.ResponseVoucher;
import com.bolttech.edirect.rulesengine.model.context.ApiContext;
import com.bolttech.edirect.rulesengine.model.voucher.Voucher;
import com.bolttech.edirect.rulesengine.service.impl.RulesService;
import com.bolttech.edirect.rulesengine.service.impl.VoucherService;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VoucherServiceTest {

        private final String API_BACKOFFICE_UI = "http://localhost:9876/vouchers/verify/v2";

        @InjectMocks
        private VoucherService voucherService;

        @Mock
        private RulesService rulesService;
        @Mock
        private RestTemplate restTemplate;


        @Test(expected = BusinessException.class)
        public void Request_backoffice_VoucherValidation_then_ThrowBusinessException() throws BusinessException {
                String MOCK_VOUCHER = "TEST1";

                when(restTemplate.postForEntity(anyString(), any(), any()))
                        .thenReturn(generateResponseEntityObject(new ResponseVoucher(
                                false,
                                "Error voucher not valid"), HttpStatus.OK));

                voucherService.validateVoucher(
                        new Voucher(MOCK_VOUCHER),
                        new ApiContext("device-protection-insurance","hk-HK-IFWD-ECOM", "moneyhero"));

        }

        @Test
        public void validatorVoucher_then_receiveTheVoucher() throws BusinessException {
                String MOCK_VOUCHER = "TEST1";
                when(restTemplate.postForEntity(anyString(), any(), any()))
                        .thenReturn(generateResponseEntityObject(MockUtil.getVoucher_MOCK_PLAYLOAD_DEVICE_MONEYHERO(),
                                        HttpStatus.OK));

                Voucher responseVoucher = voucherService.validateVoucher(
                        new Voucher(MOCK_VOUCHER),
                        new ApiContext("device-protection-insurance", "hk-HK-IFWD-ECOM", "moneyhero"));

                Assert.assertEquals(responseVoucher.getCode(), MOCK_VOUCHER);
        }

        @Test
        public void validatorVoucher_then_receiveTheVoucher_same_vertical() throws BusinessException {
                String MOCK_VOUCHER = "TEST1";
                when(restTemplate.postForEntity(anyString(), any(), any()))
                        .thenReturn(generateResponseEntityObject(MockUtil.getVoucher_MOCK_PLAYLOAD_DEVICE_MONEYHERO(),
                                HttpStatus.OK));

                Voucher responseVoucher = voucherService.validateVoucher(
                        new Voucher(MOCK_VOUCHER),
                        new ApiContext("device-protection-insurance", "hk-HK-IFWD-ECOM", "moneyhero"));

                Assert.assertTrue(responseVoucher.getVertical().contains("device-protection-insurance"));
                Assert.assertTrue(responseVoucher.getPartners().contains("moneyhero"));
        }

        @Test
        public void validatorVoucher_then_receiveTheVoucher_same_Partner() throws BusinessException {
                String MOCK_VOUCHER = "TEST1";
                when(restTemplate.postForEntity(anyString(), any(), any()))
                        .thenReturn(generateResponseEntityObject(MockUtil.getVoucher_MOCK_PLAYLOAD_DEVICE_MONEYHERO(),
                                HttpStatus.OK));

                Voucher responseVoucher = voucherService.validateVoucher(
                        new Voucher(MOCK_VOUCHER),
                        new ApiContext("device-protection-insurance", "hk-HK-IFWD-ECOM", "moneyhero"));

                Assert.assertTrue(responseVoucher.getPartners().contains("moneyhero"));
        }

        private ResponseEntity<Object> generateResponseEntityObject(ResponseVoucher response, HttpStatus httpStatus){
                return new ResponseEntity<>(response, httpStatus);
        }
}
