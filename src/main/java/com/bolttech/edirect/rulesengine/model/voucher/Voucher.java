package com.bolttech.edirect.rulesengine.model.voucher;

import java.util.List;

public class Voucher {

    private String _id;
    private String code;
    private int value;
    private String discountType;
    private int minimumAmount;
    private String startDate;
    private String endDate;
    private List<String> allowedPaymentMethods;
    private String allowedBanksDigits;
    private List<String> allowedBanks;
    private List<String> vertical;
    private Boolean single;
    private Boolean applyed;
    private List<String> types;
    private List<String> partners;
    private String applyOverTaxes;

    public Voucher() {
    }

    public Voucher(String code) {
        this.code = code;
    }

    public Voucher(String id,
                   String code,
                   int value,
                   String discountType,
                   int minimumAmount,
                   String startDate,
                   String endDate,
                   List<String> vertical,
                   String applyOverTaxes) {
        _id = id;
        this.code = code;
        this.value = value;
        this.discountType = discountType;
        this.minimumAmount = minimumAmount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.vertical = vertical;
        this.applyOverTaxes = applyOverTaxes;
    }

    public Voucher(String id,
                   String code,
                   int value,
                   String discountType,
                   int minimumAmount,
                   String startDate,
                   String endDate,
                   List<String> allowedPaymentMethods,
                   String allowedBanksDigits,
                   List<String> allowedBanks,
                   List<String> vertical,
                   Boolean single,
                   Boolean applyed,
                   List<String> types,
                   List<String> partners,
                   String applyOverTaxes) {
        _id = id;
        this.value = value;
        this.discountType = discountType;
        this.minimumAmount = minimumAmount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.allowedPaymentMethods = allowedPaymentMethods;
        this.allowedBanksDigits = allowedBanksDigits;
        this.allowedBanks = allowedBanks;
        this.vertical = vertical;
        this.single = single;
        this.applyed = applyed;
        this.types = types;
        this.partners = partners;
        this.applyOverTaxes = applyOverTaxes;
    }

    public String get_id() {
        return _id;
    }

    public String getCode() {
        return code;
    }

    public int getValue() {
        return value;
    }

    public String getDiscountType() {
        return discountType;
    }

    public int getMinimumAmount() {
        return minimumAmount;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public List<String> getAllowedPaymentMethods() {
        return allowedPaymentMethods;
    }

    public String getAllowedBanksDigits() {
        return allowedBanksDigits;
    }

    public List<String> getAllowedBanks() {
        return allowedBanks;
    }

    public List<String> getVertical() {
        return vertical;
    }

    public Boolean getSingle() {
        return single;
    }

    public Boolean getApplyed() {
        return applyed;
    }

    public List<String> getTypes() {
        return types;
    }

    public List<String> getPartners() {
        return partners;
    }

    public String getApplyOverTaxes() {
        return applyOverTaxes;
    }


}
