package com.bolttech.edirect.rulesengine.model.rules;

import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.ConsequenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Rule {

    Logger logger = LoggerFactory.getLogger(Rule.class);
    private KieSession kieSession;

    public Rule( KieSession kieSession) {
        this.kieSession = kieSession;
    }

    public Rule addData(Object o) {
        this.kieSession.insert(o);
        return this;
    }

    public void executeRule() throws BusinessException {
        try {
            this.kieSession.fireAllRules();
            this.kieSession.destroy();
        } catch (ConsequenceException e) {
            logger.error("[Rule] - ", e.getCause());
            throw new BusinessException(e.getMessage());
        }
    }

}
