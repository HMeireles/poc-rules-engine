package com.bolttech.edirect.rulesengine.model.order;

import com.bolttech.edirect.rulesengine.model.shared.Price;

public class Order {

    private Price price;
    private OrderStatusType orderStatusType;
    private Voucher voucher;
    private ShoppingCart shoppingCart;

    public Order(Price price, OrderStatusType orderStatusType, Voucher voucher) {
        this.price = price;
        this.orderStatusType = orderStatusType;
        this.voucher = voucher;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public OrderStatusType getOrderStatusType() {
        return orderStatusType;
    }

    public void setOrderStatusType(OrderStatusType orderStatusType) {
        this.orderStatusType = orderStatusType;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
}
