package com.bolttech.edirect.rulesengine.model.context;

import com.bolttech.edirect.rulesengine.model.type.InstanceType;

public class Instance {

    private InstanceType instanceType;

    public Instance() {
    }

    public Instance(String instanceType) {
        this.instanceType = InstanceType.getInstance(instanceType);
    }

    public InstanceType getInstanceType() {
        return instanceType;
    }
}
