package com.bolttech.edirect.rulesengine.model.order;

public class Voucher {

    private boolean applyOverTaxes;
    private final String code;
    private final int valuePercentage;

    public Voucher(boolean applyOverTaxes, String code, int valuePercentage) {
        this.applyOverTaxes = applyOverTaxes;
        this.code = code;
        this.valuePercentage = valuePercentage;
    }

    public boolean isApplyOverTaxes() {
        return applyOverTaxes;
    }

    public void setApplyOverTaxes(boolean applyOverTaxes) {
        this.applyOverTaxes = applyOverTaxes;
    }

    public String getCode() {
        return code;
    }

    public int getValuePercentage() {
        return valuePercentage;
    }
}
