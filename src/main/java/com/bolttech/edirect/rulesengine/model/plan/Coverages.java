package com.bolttech.edirect.rulesengine.model.plan;

public class Coverages {

    private String category;
    private String key;
    private String value;
    private String available;
    private String compareCategory;

    public Coverages() {
    }

    public Coverages(String category, String key, String value, String available, String compareCategory) {
        this.category = category;
        this.key = key;
        this.value = value;
        this.available = available;
        this.compareCategory = compareCategory;
    }

    public String getCategory() {
        return category;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getAvailable() {
        return available;
    }

    public String getCompareCategory() {
        return compareCategory;
    }
}
