package com.bolttech.edirect.rulesengine.model.rules;

import com.bolttech.edirect.rulesengine.model.type.CommandTypes;

public class Command {

    private CommandTypes commandName;
    private Boolean commandApply;

    public Command() {
    }

    public Command(CommandTypes command) {
        this.commandName = command;
        this.commandApply = false;
    }

    public String getRuleName() {
        return commandName.name();
    }

    public Boolean getRuleApply() {
        return commandApply;
    }

    public void setRuleApply(Boolean commandApply) {
        this.commandApply = commandApply;
    }

}
