package com.bolttech.edirect.rulesengine.model.plan;

public class I18n {

    private String en;
    private String hk;

    public I18n() {
    }

    public I18n(String en, String hk) {
        this.en = en;
        this.hk = hk;
    }

    public String getEn() {
        return en;
    }

    public String getHk() {
        return hk;
    }
}
