package com.bolttech.edirect.rulesengine.model.voucher;

public class VoucherCode {

    private String code;

    public VoucherCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
