package com.bolttech.edirect.rulesengine.model.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PartnerType {
    MONEYHERO("moneyhero", "moneyhero"),
    MONEYSMART("moneysmart", "moneysmart"),
    YAS("yas", "yas"),
    FZZRPH("fzzrph", "fzzrph"),
    MONEYBACKMASS("moneybackmass", "moneybackmass"),
    MONEYBACKVIP("moneybackvip", "moneybackvip"),
    CHINESEAN("chinesean", "chinesean"),
    PCG("pcg", "pcg"),
    FWD("fwd", "fwd"),
    BOLTTECH("bolttech", "bolttech");

    private final String partner;
    private final String partnerFolder;


    PartnerType(String partner, String partnerFolder) {
        this.partner = partner;
        this.partnerFolder = partnerFolder;
    }

    public String getPartnerFolder() {
        return partnerFolder;
    }

    public static PartnerType getPartnerType(String partner) {
        return PartnerType.valueOf(partner);
    }

    public String getPartner() {
        return partner;
    }

    private static Map<String, PartnerType> FORMAT_MAP = Stream
            .of(PartnerType.values())
            .collect(Collectors.toMap(s -> s.partner, Function.identity()));

    @JsonCreator // This is the factory method and must be static
    public static PartnerType fromString(String string) {
        return Optional
                .ofNullable(FORMAT_MAP.get(string))
                .orElseThrow(() -> new IllegalArgumentException(string));
    }
}
