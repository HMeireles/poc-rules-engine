package com.bolttech.edirect.rulesengine.model.plan;

import com.bolttech.edirect.rulesengine.model.shared.Price;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class Addon {

    private String name;
    private Price price;

    public Addon() {
    }

    public Addon(String name, Price price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}
