package com.bolttech.edirect.rulesengine.model.context;

import com.bolttech.edirect.rulesengine.model.type.PartnerType;

public class Partner {

    private PartnerType partnerType;

    public Partner() {
    }

    public Partner(String partnerType) {
        this.partnerType = PartnerType.getPartnerType(partnerType);
    }

    public PartnerType getPartnerType() {
        return partnerType;
    }
}
