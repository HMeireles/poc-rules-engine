package com.bolttech.edirect.rulesengine.model.order;

import com.bolttech.edirect.rulesengine.model.plan.Plan;

import java.util.List;
import java.util.Optional;

public class ShoppingCart {

    private final List<Plan> planList;

    public ShoppingCart(List<Plan> planList) {
        this.planList = planList;
    }

    public int getTotalPlans() {
        return this.planList.size();
    }

    public Optional<Plan> getPlan(int planId) {
        return Optional.empty();
    }

    public void addPlan(Plan plan) {
        this.planList.add(plan);
    }

}
