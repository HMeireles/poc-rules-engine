package com.bolttech.edirect.rulesengine.model.plan;

public class Tax {

    private double levy;
    private double eciLevy;

    public Tax() {
    }

    public double getEciLevy() {
        return eciLevy;
    }

    public void setEciLevy(double eciLevy) {
        this.eciLevy = eciLevy;
    }

    public double getLevy() {
        return levy;
    }

    public void setLevy(double levy) {
        this.levy = levy;
    }
}
