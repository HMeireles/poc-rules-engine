package com.bolttech.edirect.rulesengine.model.context;

public class ApiContext {

    private String vertical;
    private String instance;
    private String partner;

    public ApiContext() {
    }

    public ApiContext(String vertical, String instance, String partner) {
        this.vertical = vertical;
        this.instance = instance;
        this.partner = partner;
    }

    public String getVertical() {
        return vertical;
    }

    public String getInstance() {
        return instance;
    }

    public String getPartner() {
        return partner;
    }
}

