package com.bolttech.edirect.rulesengine.model.type;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VerticalType {
    CAR("car-insurance", "car"),
    CAR_CTPL("car-ctpl-insurance", "carctpl"),
    CORONA_VIRUS("corona-virus-insurance", "coronavirus"),
    DEVICE_PROTECTION("device-protection-insurance", "deviceprotection"),
    DOMESTIC_HELPER("domestic-helper-insurance", "domestichelper"),
    HEALTH("health-insurance", "health"),
    HOME_CARE("home-care-insurance", "homecare"),
    MOTORCYCLE("motorcycle-insurance", "motorcycle"),
    OVERSEAS_STUDENT("overseas-student-insurance", "overseasstudent"),
    PERSONAL_ACCIDENTS("personal-accidents-insurance", "personalaccidents"),
    PROPERTY("property-insurance", "property"),
    TRAVEL("travel-insurance", "travel");

    private final String vertical;
    private final String verticalFolder;

    VerticalType(String vertical, String verticalFolder) {
        this.vertical = vertical;
        this.verticalFolder = verticalFolder;
    }

    public String getVertical() {
        return vertical;
    }

    public static VerticalType getVertical(String vertical) {
        return VerticalType.valueOf(vertical);
    }

    public String getVerticalFolder() {
        return verticalFolder;
    }

    private static Map<String, VerticalType> FORMAT_MAP = Stream
            .of(VerticalType.values())
            .collect(Collectors.toMap(s -> s.vertical, Function.identity()));

    @JsonCreator // This is the factory method and must be static
    public static VerticalType fromString(String string) {
        return Optional
                .ofNullable(FORMAT_MAP.get(string))
                .orElseThrow(() -> new IllegalArgumentException(string));
    }
}
