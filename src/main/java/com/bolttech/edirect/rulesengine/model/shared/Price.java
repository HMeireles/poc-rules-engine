package com.bolttech.edirect.rulesengine.model.shared;

import com.bolttech.edirect.rulesengine.model.plan.PriceToView;

public class Price {

    private int providerPrice;
    private double providerPriceWithoutTax;
    private double providerTax;
    private String feeType;
/*    private Optional agentCommissionRaw;
    private Optional agentCommissionOfferedType;
    private Optional agentCommissionOfferedValue;
    private Optional agentCommissionOfferedRaw;
    private Optional agentCommissionOfferedEarned;
    private Optional agentCommissionValue;*/
    private String agentCommissionType;
    private double discount;
    private double discountValue;
    private String discountType;
    private double vat;
    private double stampDuty;
    private double originalPrice;
    private double finalPrice;
    private double partnerPrice;
    //private Optional eciLevy;
    private boolean agentRetainsCommission;
    private PriceToView formatted;

    public int getProviderPrice() {
        return providerPrice;
    }

    public double getProviderPriceWithoutTax() {
        return providerPriceWithoutTax;
    }

    public double getProviderTax() {
        return providerTax;
    }

    public String getFeeType() {
        return feeType;
    }

    public String getAgentCommissionType() {
        return agentCommissionType;
    }

    public double getDiscount() {
        return discount;
    }

    public double getDiscountValue() {
        return discountValue;
    }

    public String getDiscountType() {
        return discountType;
    }

    public double getVat() {
        return vat;
    }

    public double getStampDuty() {
        return stampDuty;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public double getPartnerPrice() {
        return partnerPrice;
    }

    public boolean isAgentRetainsCommission() {
        return agentRetainsCommission;
    }

    public PriceToView getFormatted() {
        return formatted;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public void setDiscountValue(double discountValue) {
        this.discountValue = discountValue;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Price() {
    }

    public Price(int providerPrice,
                 double providerPriceWithoutTax,
                 double providerTax,
                 String feeType,
                 String agentCommissionType,
                 double discount,
                 double discountValue,
                 String discountType,
                 double vat,
                 double stampDuty,
                 double originalPrice,
                 double finalPrice,
                 double partnerPrice,
                 boolean agentRetainsCommission,
                 PriceToView formatted) {
        this.providerPrice = providerPrice;
        this.providerPriceWithoutTax = providerPriceWithoutTax;
        this.providerTax = providerTax;
        this.feeType = feeType;
        this.agentCommissionType = agentCommissionType;
        this.discount = discount;
        this.discountValue = discountValue;
        this.discountType = discountType;
        this.vat = vat;
        this.stampDuty = stampDuty;
        this.originalPrice = originalPrice;
        this.finalPrice = finalPrice;
        this.partnerPrice = partnerPrice;
        this.agentRetainsCommission = agentRetainsCommission;
        this.formatted = formatted;
    }
}
