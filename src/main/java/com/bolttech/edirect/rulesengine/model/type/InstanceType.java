package com.bolttech.edirect.rulesengine.model.type;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public enum InstanceType {
    TH_FRANK("th-TH","thfrank"),
    HONK_BROCK("hk-HK-IBP", "hkbrock"),
    HONK_GI("hk-HK-IFWD-ECOM", "honkgi"),
    VIETNAM("vi-VN", "vietnam");

    private final String instance;
    private final String rulesFolder;

    InstanceType(String instance, String rulesFolder) {
        this.instance = instance;
        this.rulesFolder = rulesFolder;
    }

    public String getInstance() {
        return instance;
    }

    public static InstanceType getInstance(String instance) {
        return InstanceType.valueOf(instance);
    }

    public String getRulesFolder() {
        return this.rulesFolder;
    }

    public static List<String> getAllInstance() {
        List<String> instances = new ArrayList<>();

        for (InstanceType iType : InstanceType.values()) {
            instances.add(iType.getInstance());
        }

        return instances;
    }

    private static Map<String, InstanceType> FORMAT_MAP = Stream
            .of(InstanceType.values())
            .collect(Collectors.toMap(s -> s.instance, Function.identity()));

    @JsonCreator // This is the factory method and must be static
    public static InstanceType fromString(String string) {
        return Optional
                .ofNullable(FORMAT_MAP.get(string))
                .orElseThrow(() -> new IllegalArgumentException(string));
    }

}
