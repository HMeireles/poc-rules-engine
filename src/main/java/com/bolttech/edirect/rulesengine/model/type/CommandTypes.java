package com.bolttech.edirect.rulesengine.model.type;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CommandTypes {
    DEFAULT_VOUCHER_VALIDATOR("default_voucher_validator"),
    DEFAULT_VOUCHER_CALCULATOR("default_voucher_calculator");

    private String command;

    CommandTypes(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public static List<String> getAllActionsTypes() {
        return Stream.of(CommandTypes.values())
                .map(CommandTypes::getCommand)
                .collect(Collectors.toList());
    }

    public static CommandTypes getActionTypeFromRule(String command) {
        return CommandTypes.valueOf(command);
    }

    public static boolean isCommand(String command) {
        return Arrays.stream(CommandTypes.values())
                .anyMatch(commandTypes -> commandTypes.getCommand().equals(command));
    }

}
