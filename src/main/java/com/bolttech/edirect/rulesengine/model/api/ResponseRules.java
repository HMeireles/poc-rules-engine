package com.bolttech.edirect.rulesengine.model.api;

import com.bolttech.edirect.rulesengine.model.quote.Quote;
import com.bolttech.edirect.rulesengine.model.plan.Plan;

import java.util.List;

public class ResponseRules {

    private boolean success;
    private String error;
    private Quote quote;
    private List<Plan> plans;

    public ResponseRules() {
    }


    public boolean isSuccess() {
        return success;
    }

    public ResponseRules setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public ResponseRules setError(String error) {
        this.error = error;
        return this;
    }

    public ResponseRules setQuote(Quote quote) {
        this.quote = quote;
        return this;
    }

    public ResponseRules setPlans(List<Plan> plans) {
        this.plans = plans;
        return this;
    }

    public String getError() {
        return error;
    }

    public Quote getQuote() {
        return quote;
    }

    public List<Plan> getPlans() {
        return plans;
    }
}
