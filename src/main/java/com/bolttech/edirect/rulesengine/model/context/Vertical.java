package com.bolttech.edirect.rulesengine.model.context;

import com.bolttech.edirect.rulesengine.model.type.VerticalType;

public class Vertical {

    private VerticalType verticalType;

    public Vertical() {
    }

    public Vertical(String verticalType) {
        this.verticalType = VerticalType.getVertical(verticalType);
    }

    public VerticalType getVerticalType() {
        return verticalType;
    }
}
