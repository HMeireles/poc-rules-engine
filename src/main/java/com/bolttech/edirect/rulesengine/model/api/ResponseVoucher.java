package com.bolttech.edirect.rulesengine.model.api;

import com.bolttech.edirect.rulesengine.model.voucher.Voucher;

public class ResponseVoucher {

    private boolean success;
    private Voucher voucher;
    private String message;

    public ResponseVoucher() {
    }

    public ResponseVoucher(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ResponseVoucher(boolean success, Voucher voucher, String message) {
        this.success = success;
        this.voucher = voucher;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public String getMessage() {
        return message;
    }
}
