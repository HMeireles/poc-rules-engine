package com.bolttech.edirect.rulesengine.model.api;

import com.bolttech.edirect.rulesengine.model.context.ApiContext;
import com.bolttech.edirect.rulesengine.model.plan.Plan;
import com.bolttech.edirect.rulesengine.model.quote.Quote;
import com.bolttech.edirect.rulesengine.model.voucher.Voucher;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.List;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class RequestRules {

    private List<String> commands;
    private List<Plan> plans;
    private ApiContext apiContext;
    private Quote quote;
    private Voucher voucher;

    public RequestRules() {
    }

    public RequestRules(List<String> commands, Quote quote, ApiContext apiContext) {
        this.commands = commands;
        this.quote = quote;
        this.apiContext = apiContext;
    }

    public RequestRules(List<String> commands, List<Plan> plans, Voucher voucher, ApiContext apiContext) {
        this.commands = commands;
        this.plans = plans;
        this.voucher = voucher;
        this.apiContext = apiContext;
    }

    public RequestRules(List<String> commands, List<Plan> plans, Quote quote, Voucher voucher, ApiContext apiContext) {
        this.commands = commands;
        this.plans = plans;
        this.quote = quote;
        this.voucher = voucher;
        this.apiContext = apiContext;
    }

    public List<String> getCommands() {
        return commands;
    }

    public List<Plan> getPlans() {
        return plans;
    }

    public Quote getQuote() {
        return quote;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public List<String> getCommand() {
        return commands;
    }

    public ApiContext getApiContext() {
        return apiContext;
    }
}
