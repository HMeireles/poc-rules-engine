package com.bolttech.edirect.rulesengine.model.quote;

import com.bolttech.edirect.rulesengine.model.context.Partner;
import com.bolttech.edirect.rulesengine.model.plan.Plan;
import com.bolttech.edirect.rulesengine.model.type.InstanceType;
import com.bolttech.edirect.rulesengine.model.type.VerticalType;
import com.bolttech.edirect.rulesengine.model.voucher.Voucher;

public class Quote {

    private int id;
    private VerticalType vertical;
    private Plan plan;
    private InstanceType country;
    private Partner partner;
    private Voucher voucher;

    public Quote() {
    }

    public Quote(int id,
                 VerticalType vertical,
                 Plan plan,
                 InstanceType country,
                 Partner partner, Voucher voucher) {
        this.id = id;
        this.vertical = vertical;
        this.plan = plan;
        this.country = country;
        this.partner = partner;
        this.voucher = voucher;
    }

    public int getId() {
        return id;
    }

    public VerticalType getVertical() {
        return vertical;
    }

    public Plan getPlan() {
        return plan;
    }

    public InstanceType getCountry() {
        return country;
    }

    public Partner getPartner() {
        return partner;
    }

    public Voucher getVoucher() {
        return voucher;
    }

}
