package com.bolttech.edirect.rulesengine.model.order;

public enum OrderStatusType {
    CREATED,
    VALIDATION,
    CONFIRMED
}
