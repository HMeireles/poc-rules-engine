package com.bolttech.edirect.rulesengine.model.plan;

import com.bolttech.edirect.rulesengine.model.shared.Price;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.List;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class Plan {

    private String id;
    private String internalId;
    private I18n name;
    private String type;
    private Price price;
    private String insuranceProvider;
    private List<Coverages> coverages;
    private String planType;
    private String packageName;
    private String planCode;
    private String purchasePrice;
    private String provider;

    public Plan() {
    }

    public Plan(String id, String internalId, I18n name, String type, Price price, String insuranceProvider, List<Coverages> coverages, String planType, String packageName, String planCode, String purchasePrice, String provider) {
        this.id = id;
        this.internalId = internalId;
        this.name = name;
        this.type = type;
        this.price = price;
        this.insuranceProvider = insuranceProvider;
        this.coverages = coverages;
        this.planType = planType;
        this.packageName = packageName;
        this.planCode = planCode;
        this.purchasePrice = purchasePrice;
        this.provider = provider;
    }

    public String getId() {
        return id;
    }

    public String getInternalId() {
        return internalId;
    }

    public I18n getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Price getPrice() {
        return price;
    }

    public String getInsuranceProvider() {
        return insuranceProvider;
    }

    public List<Coverages> getCoverages() {
        return coverages;
    }

    public String getPlanType() {
        return planType;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getPlanCode() {
        return planCode;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public String getProvider() {
        return provider;
    }

}
