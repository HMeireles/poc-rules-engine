package com.bolttech.edirect.rulesengine.model.plan;

public class PriceToView {

    private String stampDuty;
    private String vat;
    private String providerPrice;
    private String providerPriceWithoutTax;
    private String providerTax;
    private String agentCommissionOfferedEarned;
    private String originalPrice;
    private String finalPrice;
    private String partnerPrice;

    public PriceToView() {
    }

    public PriceToView(String stampDuty, String vat, String providerPrice, String providerPriceWithoutTax, String providerTax, String agentCommissionOfferedEarned, String originalPrice, String finalPrice, String partnerPrice) {
        this.stampDuty = stampDuty;
        this.vat = vat;
        this.providerPrice = providerPrice;
        this.providerPriceWithoutTax = providerPriceWithoutTax;
        this.providerTax = providerTax;
        this.agentCommissionOfferedEarned = agentCommissionOfferedEarned;
        this.originalPrice = originalPrice;
        this.finalPrice = finalPrice;
        this.partnerPrice = partnerPrice;
    }

    public String getStampDuty() {
        return stampDuty;
    }

    public String getVat() {
        return vat;
    }

    public String getProviderPrice() {
        return providerPrice;
    }

    public String getProviderPriceWithoutTax() {
        return providerPriceWithoutTax;
    }

    public String getProviderTax() {
        return providerTax;
    }

    public String getAgentCommissionOfferedEarned() {
        return agentCommissionOfferedEarned;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public String getPartnerPrice() {
        return partnerPrice;
    }
}
