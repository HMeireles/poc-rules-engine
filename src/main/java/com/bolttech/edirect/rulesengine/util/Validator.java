package com.bolttech.edirect.rulesengine.util;

import com.bolttech.edirect.rulesengine.model.context.ApiContext;
import com.bolttech.edirect.rulesengine.model.type.CommandTypes;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;

import java.time.LocalDate;
import java.util.List;

public class Validator {

    public Validator() throws InstantiationException {
        throw new InstantiationException("This call can be instantiated!");
    }

    public static boolean commands(List<String> commandsList) {
        for (String inCameCommand : commandsList) {
            if (!CommandTypes.isCommand(inCameCommand)) {
                return false;
            }
        }
        return true;
    }

    public static void apiContext(ApiContext apiContext) throws BusinessException {
        if (apiContext == null) {
            throw new BusinessException("Api context can´t be null");
        }
        if (apiContext.getInstance() == null) {
            throw new BusinessException("Api context instance can´t be null");
        }
        if (apiContext.getVertical() == null) {
            throw new BusinessException("Api context vertical can´t be null");
        }
        if (apiContext.getPartner() == null) {
            throw new BusinessException("Api context partner can´t be null");
        }
    }

    public static boolean isExpiredVoucher(String startDate, String endDate) {
        if(startDate.length() < 9 || endDate.length() < 9) {
            return true;
        }
        LocalDate localDate = LocalDate.now();

        LocalDate startDateConverted = LocalDate.parse(startDate.substring(0,10));
        LocalDate endDateConverted = LocalDate.parse(endDate.substring(0,10));

        boolean isPassedDate = localDate.isAfter(endDateConverted);
        boolean isDateOntRage = localDate.isAfter(startDateConverted) && localDate.isBefore(endDateConverted);

        return isPassedDate || !isDateOntRage;
    }
}
