package com.bolttech.edirect.rulesengine.mocks;

import com.bolttech.edirect.rulesengine.model.api.ResponseVoucher;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import static com.bolttech.edirect.rulesengine.mocks.MockBackofficeResponse.MOCK_PLAYLOAD_DEVICE_MONEYHERO;

public class MockUtil {

    public static ResponseVoucher getVoucher_MOCK_PLAYLOAD_DEVICE_MONEYHERO() {
        ObjectMapper mapper = new ObjectMapper();
        ResponseVoucher rv = null;
        try {
            rv = mapper.readValue(MOCK_PLAYLOAD_DEVICE_MONEYHERO, ResponseVoucher.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return rv;
    }
}
