package com.bolttech.edirect.rulesengine.mocks;

public class MockBackofficeResponse {

    public MockBackofficeResponse() throws InstantiationException {
        throw new InstantiationException("This class can´t be instantiate.");
    }

    public static final String MOCK_PLAYLOAD_DEVICE_MONEYHERO = "{\n" +
            "    \"success\": true,\n" +
            "    \"voucher\": {\n" +
            "        \"discountType\": \"1\",\n" +
            "        \"minimumAmount\": 0,\n" +
            "        \"allowedPaymentMethods\": [],\n" +
            "        \"allowedBanks\": [],\n" +
            "        \"vertical\": [\n" +
            "            \"device-protection-insurance\"\n" +
            "        ],\n" +
            "        \"single\": false,\n" +
            "        \"applyed\": false,\n" +
            "        \"types\": [],\n" +
            "        \"partners\": [\n" +
            "            \"moneyhero\"\n" +
            "        ],\n" +
            "        \"applyOverTaxes\": false,\n" +
            "        \"_id\": \"5faabc898c5bdc6be6215472\",\n" +
            "        \"value\": 10,\n" +
            "        \"code\": \"TEST1\",\n" +
            "        \"startDate\": \"2020-11-10T00:00:00.000Z\",\n" +
            "        \"endDate\": \"2020-11-28T00:00:00.000Z\"\n" +
            "    },\n" +
            "    \"message\": \"Voucher valid to use!\"\n" +
            "}";
}
