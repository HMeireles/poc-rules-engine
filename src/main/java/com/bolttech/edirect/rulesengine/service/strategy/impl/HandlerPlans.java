package com.bolttech.edirect.rulesengine.service.strategy.impl;

import com.bolttech.edirect.rulesengine.model.api.RequestRules;
import com.bolttech.edirect.rulesengine.model.api.ResponseRules;
import com.bolttech.edirect.rulesengine.model.plan.Plan;
import com.bolttech.edirect.rulesengine.model.voucher.Voucher;
import com.bolttech.edirect.rulesengine.service.impl.RulesService;
import com.bolttech.edirect.rulesengine.service.impl.VoucherService;
import com.bolttech.edirect.rulesengine.service.strategy.GatewayStrategy;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;

import java.util.List;

public class HandlerPlans implements GatewayStrategy {

    @Override
    public ResponseRules executeStrategy(RequestRules request, VoucherService voucherService, RulesService rulesService) throws BusinessException {
        Voucher voucher = voucherService.validateVoucher(request.getVoucher(), request.getApiContext());

        List<Plan> planList = rulesService.applyVoucherToPlans(request.getPlans(), voucher, request.getApiContext());

        return new ResponseRules().setSuccess(true).setPlans(planList);
    }
}
