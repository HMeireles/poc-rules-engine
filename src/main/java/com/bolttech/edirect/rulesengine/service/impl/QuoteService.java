package com.bolttech.edirect.rulesengine.service.impl;

import com.bolttech.edirect.rulesengine.model.quote.Quote;
import com.bolttech.edirect.rulesengine.model.rules.Rule;
import com.bolttech.edirect.rulesengine.service.RulesService;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@Service
public class QuoteService implements RulesService<Quote> {

    public QuoteService() {
    }

    @Override
    public List<Quote> executeRules(List<Quote> list, List<Rule> ruleList) throws NotImplementedException {
        throw new NotImplementedException();
    }

    @Override
    public Quote executeRules(Quote quote, List<Rule> ruleList) throws NotImplementedException {
        throw new NotImplementedException();
    }
}
