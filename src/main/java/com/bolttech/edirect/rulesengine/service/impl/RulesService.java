package com.bolttech.edirect.rulesengine.service.impl;

import com.bolttech.edirect.rulesengine.model.context.ApiContext;
import com.bolttech.edirect.rulesengine.model.plan.Plan;
import com.bolttech.edirect.rulesengine.model.rules.Rule;
import com.bolttech.edirect.rulesengine.model.type.CommandTypes;
import com.bolttech.edirect.rulesengine.model.type.InstanceType;
import com.bolttech.edirect.rulesengine.model.type.PartnerType;
import com.bolttech.edirect.rulesengine.model.type.VerticalType;
import com.bolttech.edirect.rulesengine.model.voucher.Voucher;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;
import org.kie.api.runtime.KieContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RulesService {

    @Autowired
    private KieContainer kieContainer;

    public void validateRuleVoucher(Voucher voucher, ApiContext apiContext) throws BusinessException {
        new Rule(kieContainer.newKieSession(CommandTypes.DEFAULT_VOUCHER_VALIDATOR.getCommand()))
                .addData(apiContext)
                .addData(voucher)
                .executeRule();
    }

    public List<Plan> applyVoucherToPlans(List<Plan> planList, Voucher voucher, ApiContext apiContext) throws BusinessException {
        List<Plan> tempPlanList = new ArrayList<>();

        for (Plan plan : planList) {
            new Rule(kieContainer.newKieSession(CommandTypes.DEFAULT_VOUCHER_CALCULATOR.getCommand()))
                    .addData(apiContext)
                    .addData(voucher)
                    .addData(plan)
                    .executeRule();

            tempPlanList.add(plan);
        }

        return tempPlanList;
    }

    private String getRuleNameByApiContext(ApiContext apiContext) throws BusinessException {
        StringBuilder ruleNameBuilder = new StringBuilder();
        final String SEPARATOR = "_";

        return  ruleNameBuilder
                .append(InstanceType.getInstance(apiContext.getInstance()).getRulesFolder())
                .append(SEPARATOR)
                .append(VerticalType.getVertical(apiContext.getVertical()).getVerticalFolder())
                .append(SEPARATOR)
                .append(PartnerType.getPartnerType(apiContext.getPartner()).getPartnerFolder())
                .append(SEPARATOR)
                .toString();
    }


}
