package com.bolttech.edirect.rulesengine.service;

import com.bolttech.edirect.rulesengine.model.rules.Rule;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public interface RulesService<T> {

    List<T> executeRules(List<T> list, List<Rule> ruleList) throws NotImplementedException;

    T executeRules(T t, List<Rule> ruleList) throws NotImplementedException;

}
