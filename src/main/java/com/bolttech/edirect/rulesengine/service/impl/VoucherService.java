package com.bolttech.edirect.rulesengine.service.impl;

import com.bolttech.edirect.rulesengine.model.api.ResponseVoucher;
import com.bolttech.edirect.rulesengine.model.context.ApiContext;
import com.bolttech.edirect.rulesengine.model.voucher.Voucher;
import com.bolttech.edirect.rulesengine.model.voucher.VoucherCode;
import com.bolttech.edirect.rulesengine.service.VoucherApi;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class VoucherService implements VoucherApi {

    private final String API_BACKOFFICE_UI = "http://localhost:9876/vouchers/verify/v2";

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private RulesService rulesService;



    @Override
    public Voucher validateVoucher(Voucher voucher, ApiContext apiContext) throws BusinessException {
        ResponseEntity<ResponseVoucher> responseEntity = restTemplate.postForEntity(API_BACKOFFICE_UI, new VoucherCode(voucher.getCode()), ResponseVoucher.class);

        if (responseEntity == null) {
            throw new BusinessException("Request to backoffice fail.");
        }
        ResponseVoucher responseVoucher = responseEntity.getBody();
        //ResponseVoucher responseVoucher = getVoucher_MOCK_PLAYLOAD_DEVICE_MONEYHERO();

        if (!responseVoucher.isSuccess()) {
            throw new BusinessException(responseVoucher.getMessage());
        }

        rulesService.validateRuleVoucher(responseVoucher.getVoucher(), apiContext);

        return responseVoucher.getVoucher();
    }



}