package com.bolttech.edirect.rulesengine.service.impl;

import com.bolttech.edirect.rulesengine.model.plan.Plan;
import com.bolttech.edirect.rulesengine.model.rules.Rule;
import com.bolttech.edirect.rulesengine.service.RulesService;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@Service
public class PlanService implements RulesService<Plan> {

    public PlanService() {
    }

    @Override
    public List<Plan> executeRules(List<Plan> planList, List<Rule> ruleList) {
        return null;
    }

    @Override
    public Plan executeRules(Plan plan, List<Rule> ruleList) throws NotImplementedException {
        throw new NotImplementedException();
    }
}
