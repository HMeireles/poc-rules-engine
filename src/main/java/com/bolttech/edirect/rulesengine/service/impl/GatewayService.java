package com.bolttech.edirect.rulesengine.service.impl;

import com.bolttech.edirect.rulesengine.model.api.RequestRules;
import com.bolttech.edirect.rulesengine.model.api.ResponseRules;
import com.bolttech.edirect.rulesengine.service.strategy.HandlerType;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;
import com.bolttech.edirect.rulesengine.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GatewayService {

    @Autowired
    private PlanService planService;
    @Autowired
    private RulesService rulesService;
    @Autowired
    private QuoteService quoteService;
    @Autowired
    private VoucherService voucherService;


    /**
     * This function will make the separation of receive request with quote or not
     * and call the service for that flow.
     *
     * TODO make the validation if rules have bean apply
     *
     * @param request RulesRequest
     * @return RulesResponse
     */
    public ResponseRules handleRequest(RequestRules request) {
        ResponseRules responseRules = new ResponseRules();
        try {
            Validator.apiContext(request.getApiContext());

            // apply voucher to plans default strategy
            if (request.getVoucher() != null && request.getPlans().size() != 0) {
               return HandlerType.PLANS.getHandler().executeStrategy(request, voucherService, rulesService);
            }

            if (request.getQuote() != null) {
               return HandlerType.QUOTE.getHandler().executeStrategy(request, voucherService, rulesService);
            }


        } catch (BusinessException ex) {
            return responseRules.setSuccess(false).setError(ex.getMessage());
        }

        return responseRules.setSuccess(false).setError("No rule apply for your request");
    }



}
