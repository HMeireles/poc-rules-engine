package com.bolttech.edirect.rulesengine.service.strategy;

import com.bolttech.edirect.rulesengine.service.strategy.impl.HandlerPlans;
import com.bolttech.edirect.rulesengine.service.strategy.impl.HandlerQuote;

public enum HandlerType {
    PLANS(new HandlerPlans()),
    QUOTE(new HandlerQuote());

    private final GatewayStrategy handler;

    HandlerType(GatewayStrategy handler) {
        this.handler = handler;
    }

    public GatewayStrategy getHandler() {
        return handler;
    }
}
