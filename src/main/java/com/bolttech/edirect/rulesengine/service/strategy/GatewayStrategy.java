package com.bolttech.edirect.rulesengine.service.strategy;

import com.bolttech.edirect.rulesengine.model.api.RequestRules;
import com.bolttech.edirect.rulesengine.model.api.ResponseRules;
import com.bolttech.edirect.rulesengine.service.impl.RulesService;
import com.bolttech.edirect.rulesengine.service.impl.VoucherService;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;

public interface GatewayStrategy {

    ResponseRules executeStrategy(RequestRules requestRules, VoucherService voucherService, RulesService rulesService) throws BusinessException;

}
