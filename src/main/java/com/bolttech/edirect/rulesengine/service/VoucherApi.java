package com.bolttech.edirect.rulesengine.service;

import com.bolttech.edirect.rulesengine.model.context.ApiContext;
import com.bolttech.edirect.rulesengine.model.voucher.Voucher;
import com.bolttech.edirect.rulesengine.shared.errors.BusinessException;

public interface VoucherApi {

    Voucher validateVoucher(Voucher voucher, ApiContext apiContext) throws BusinessException;

}
