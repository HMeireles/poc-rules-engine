package com.bolttech.edirect.rulesengine.controller;

import com.bolttech.edirect.rulesengine.model.api.RequestRules;
import com.bolttech.edirect.rulesengine.model.api.ResponseRules;
import com.bolttech.edirect.rulesengine.service.impl.GatewayService;
import com.bolttech.edirect.rulesengine.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RulesController {

    private final GatewayService gatewayService;


    @Autowired
    public RulesController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @RequestMapping(value = "/gateway", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<ResponseRules> runRule(@RequestBody RequestRules request) {
        boolean isValidRules = Validator.commands(request.getCommands());

        if (!isValidRules) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        ResponseRules response = gatewayService.handleRequest(request);

        if (response.isSuccess()) {
          return new ResponseEntity<ResponseRules>(response, HttpStatus.OK);
        }

        return new ResponseEntity<ResponseRules>(response, HttpStatus.FAILED_DEPENDENCY);
    }

}
