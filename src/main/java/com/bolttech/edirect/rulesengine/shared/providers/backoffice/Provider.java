package com.bolttech.edirect.rulesengine.shared.providers.backoffice;

public interface Provider<T> {

    T create(T t);

    T read(T t);

}
