package com.bolttech.edirect.rulesengine.shared.errors;

public class BusinessException extends Exception {

    public BusinessException(String message) {
        super(message);
    }
}
